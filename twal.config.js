module.exports = {
  i18n: {
    namespaces: ['nav-links', 'museum']
  },
  routing: {
    linksNamespace: 'nav-links',
    routes: [{ component: 'Museum', default: true }]
  },
  build: {
    folderName: 'cordova',
    id: 'io.twal.demo',
    fullScreen: true,
    orientation: 'landscape',
    platforms: [{ name: 'android', spec: '@7.1.4' }, { name: 'browser', spec: '@latest' }]
  }
};
